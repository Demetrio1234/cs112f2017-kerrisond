//**************
// Guessing Game Program
// *************
import java.util.Date;
import java.util.Random;
import java.util.Scanner;
public class GuessmyNumber
{
   public static void main(String[]args){
       System.out.println("David Nadel Practical #2" + new Date());
       Random rand = new Random();
       int numberGuessed = rand.nextInt(100);
       int tries = 0;
       int guess;
       Scanner scan = new Scanner(System.in);
           while (true){
               System.out.println("Welcome to the Guessing Game!");
               System.out.println("Enter a number between 1 and 100.");
               guess = scan.nextInt();
               tries++;                if (guess == numberGuessed) {
                   break;
               }
               else if (guess < numberGuessed) {
                   System.out.println("Too Low!");                }
               else if (guess > numberGuessed) {
                   System.out.println("Too High!");                }
   }                    System.out.println("You have won the game!");
                   System.out.println("The number was " + numberGuessed + ".");
                   System.out.println("It took you " + tries + " tries.");
               }
}



